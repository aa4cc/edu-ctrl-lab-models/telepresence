/*
* Generated S-function Target for model CheckInvDiffKin. 
* This file provides access to the generated S-function target
* export file for other models.
*
* Created: Fri Feb  4 12:47:30 2022
*/

#ifndef RTWSFCN_CheckInvDiffKin_sf_H
#define RTWSFCN_CheckInvDiffKin_sf_H

#include "CheckInvDiffKin_sfcn_rtw\CheckInvDiffKin_sf.h"
  #include "CheckInvDiffKin_sfcn_rtw\CheckInvDiffKin_sf_private.h"

#endif
