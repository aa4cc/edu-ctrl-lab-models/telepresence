# Quanser Telepresence System

This is a repository with supporting material (texts, data, codes, ...) for [Quanser Telepresence System](https://www.quanser.com/products/telepresence/).

![Quanser Telepresence System](figures/quanser_telepresence.jpg)

The telepresence system is actually comprised of two standalone products: [HD² High Definition Haptic Device](https://www.quanser.com/products/hd2-high-definition-haptic-device/) and [QArm](https://www.quanser.com/products/qarm/).

While this telepresence system was offered (and sold) as a single product, the interoperability of the two devices is not particularly documented by the producer... At least not in the materials we had access to.  